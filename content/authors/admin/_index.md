---
title: Tinsae Gebrechristos Dulecha
role: Research Fellow, University of Verona
avatar_filename: tinsufam.jpg
bio: ""
interests:
  - Artificial Intelligence
  - Machine Learning
  - Measurement
  - analysis
  - and modeling of material appearance
social:
  - icon: envelope
    icon_pack: fas
    link: /#contact
  - icon: graduation-cap
    icon_pack: fas
    link: https://scholar.google.it/citations?user=BQttuuoAAAAJ&hl=en
  - icon: github
    icon_pack: fab
    link: https://github.com/gcushen
  - icon: linkedin.com/in/tinsae-gebrechristos-dulecha-b2ba7261
    icon_pack: fab
    link: https://www.linkedin.com/
organizations:
  - name: University of Verona
    url: https://www.univr.it/en/home
education:
  courses:
    - course: PhD in Computer Science
      institution: University of Verona
      year: 2021
    - course: MSc in Computer Science
      institution: Ca'Foscari University of Venice
      year: 2010
    - course: BSc in Computer Science
      institution: Hawassa University
      year: 2007
email: tinsae.gebrechristos@gmail.com
superuser: true
highlight_name: true
---
<!--StartFragment-->

Tinsae Gebrechristos Dulecha received a BSc degree in computer science from Hawassa University, Ethiopia,  in 2007, he then worked at Ethio telecom for 5 years till he joined Ca'Foscari University (October 2012) where he received his MSc in Computer Science in October 2014. December 2014, he won a 1-year research fellow to work on a project entitled "Augmented reality framework for museums" at IUAV University of Venice, Department of culture. In March 2015 he joined Hawassa University, Institute of Technology, School of informatics, and served as a lecturer. In October 2016, he won a 1-year research fellow to work on a project entitled "Automated inventory of a cooler" at Ca'Foscari University, European Center for Living Technology(ECLT). On June 2021 he received  Ph.D. in computer science from the University of  Verona.  His research interests are in the areas of machine learning, deep learning and measurement, analysis, and modeling of material appearance. 

<!--EndFragment-->