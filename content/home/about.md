---
widget: about
widget_id: A bit about me
headless: true
weight: 20
title: "I received a BSc degree in computer science from Hawassa University,
  Ethiopia, in 2007. Then I joined ethio telecom, formerly known as Ethiopian
  Telecommunications Corporation (ETC), and worked as Junior Programmer,
  Programmer and Pre-sales expert. After having 5 years of experience, in
  October 2012, I joined Ca'Foscari University of Venice, Italy, and received
  MSc in Computer Science in October 2014. After my MSc, I won a 1-year research
  fellow to work on a project entitled \"Augmented reality framework for
  museums\" at IUAV University of Venice, Department of culture. In March 2015,
  after one year of research experience, I joined Hawassa University, Institute
  of Technology, School of informatics, and served as a lecturer. During my stay
  at Hawassa university, I taught a practical part of an introduction to
  information storage and retrieval for undergraduate students. In October 2016,
  I won a 1-year research fellow to work on a project entitled \"Automated
  inventory of a cooler\" at Ca'Foscari University, European Center for Living
  Technology (ECLT). In October 2017, I joined the University of Verona to
  pursue Ph.D. and received Ph.D. in Computer Science on June 16, 2021. During
  my Ph.D. study, I had used different deep learning and machine learning
  techniques to solve different material appearance analysis problems. My main
  research interests are in the areas of machine learning, deep learning and
  measurement, analysis, and modeling of material appearance.         "
active: true
author: admin
design:
  background:
    image: tinsae-min.png
---
